<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends AdminController {
  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->auth();
    // Configure limits on our controller methods
    // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
    $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
    $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
    $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    $this->load->model('M_main');
    $this->load->model('Category');
    $this->load->database();
    $this->load->library(array('ion_auth', 'form_validation'));
    $this->load->helper(array('url', 'language'));
    $this->load->library('email');

  }

	public function test_get()
	{
        $theCredential = $this->user_data;
        $this->response($theCredential, 200); // OK (200) being the HTTP response code
    }
    public function create_category_post(){
        $date = new DateTime();
        $date = $date->format('Y-m-d h:m:s');
        $name = $this->post('name');
        $slug = $this->post('slug');
        $order = '1';
        $insert_data = array(
          'name'=>$name,
          'order'=>$order,
          'slug'=>$slug,
          'created_at'=>$date,
        );
        $this->form_validation->set_rules(
            'name', $this->lang->line('create_user_validation_email_label'),
            'trim|required|is_unique[categories.name]');
        $this->form_validation->set_data($this->post());
        if ($this->form_validation->run() === TRUE)
        {
            $query  = $this->Category->create($insert_data);
            if($query)
            {
                $data = array([
                   'status' => 'Insert Category Success!'
                ]);
                $this->response($data, 200); // 200 being the HTTP response code
            } else
            {
                $data = array([
                    'status' => 'Insert Category failed!'
                 ]);
                $this->response($data, 404);
            }
        }else{
            $invalid = ['status' => 'Name is exist'];
            $this->response($invalid,  REST_Controller::HTTP_BAD_REQUEST);//400
        }
    }

    function get_all_category_get(){
        $data = $this->Category->get_list();
        $this->response($data,REST_Controller::HTTP_OK);
    }

    function edit_category_put($id_update){
        $date = new DateTime();
        $date = $date->format('Y-m-d h:m:s');
        $check = $this->Category->check_data_exists($id_update);
        $name = $this->put('name');
        $slug = $this->post('slug');
        $order = '1';
        $update_data = array(
          'name'=>$name,
          'order'=>$order,
          'slug'=>$slug,
          'updated_at' => $date,
        );
        if($check->name == $name && $check->slug == $slug){
            $data = array([
                'status' => 'Update Category Success!'
            ]);
            $this->response($data, 200); // 200 being the HTTP response code
        }else{
            var_dump('sai');
        }
        var_dump($check->name == $name);
        exit;
        // $this->form_validation->set_rules(
        //     'name', $this->lang->line('create_user_validation_email_label'),
        //     'trim|required|is_unique[categories.name]');
        // $this->form_validation->set_data($this->put());
        // if ($this->form_validation->run() === TRUE)
        // {
        $query  = $this->Category->update($id_update,$update_data);
        if($query)
        {
            $data = array([
                'status' => 'Update Category Success!'
            ]);
            $this->response($data, 200); // 200 being the HTTP response code
        } else
        {
            $data = array([
                'status' => 'Update Category failed!'
            ]);
            $this->response($data, REST_Controller::HTTP_BAD_REQUEST);
        }
        // }else{
        //     $data = array([
        //         'status' => 'Update Category failed!'
        //     ]);
        //     $this->response($data, 404);
        // }
    }
    function delete_category_delete($id){

        var_dump($this->Category->check_data_exists($id));
        exit;
        if($this->Category->check_data_exists($id)){

            if($this->Category->delete($id)){
                $data = array([
                    'status' => 'Delete Category Success!'
                ]);
                //Em dat day 404 la ko dung
                //data o day tra ve ko dung
                $this->response($data, 200);
            }else{
                $data = array([
                    'status' => 'Delete Category failed!'
                ]);
                $this->response($data,  REST_Controller::HTTP_BAD_REQUEST);
            }
        }else{
            $data = array([
                'status' => 'This id not exists!'
            ]);
            $this->response($data,  REST_Controller::HTTP_BAD_REQUEST);
        }
    }

}
