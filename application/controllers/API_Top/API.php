<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class API extends BD_Controller {
  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    // Configure limits on our controller methods
    // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
    $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
    $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
    $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    $this->load->model('Project');
    $this->load->model('Pickup');
    $this->load->model('News');
    $this->load->model('Favourite');
    $this->load->database();
    $this->load->library(array('ion_auth', 'form_validation'));
    $this->load->helper(array('url', 'language'));
    $this->load->library('email');

  }

	public function pickup_projects_get(){
        $input['join'] = array('pickup_projects','pickup_projects.project_id = projects.id');
        $input['limit'] = array(8,0);
        $input['order'] = array('created','DESC');
        $toppage = $this->Project->joinroom($input);
        if($toppage){
            $this->response($toppage, 200); 
        }else{
            $data = array([
                'status' => ' Get failed!'
            ]);
            $this->response($data, 400); 
        }
    }

    public function new_project_get(){
        $input['order'] = array('collection_start_date','DESC');
        $input['limit'] = array(4,0);
        $data = $this->Project->get_list($input);
        $this->response($data, 200); 
 
    }

    public function favourite_projects_get(){
        $input['join'] = array('favourite_projects','favourite_projects.project_id = projects.id');
        $input['limit'] = array(8,0);
        $input['order'] = array('created','DESC');
        $favourite = $this->Project->joinroom($input);
        if($favourite){
            $this->response($favourite, 200); 
        }else{
            $data = array([
                'status' => ' Get failed!'
            ]);
            $this->response($data, 400); 
        }
    }
    public function news_get(){
        $input['order'] = array('created','DESC');
        $input['limit'] = array(4,0);
        $data = $this->News->get_list($input);
        $this->response($data, 200); 
    }

    public function nominations_projects_get(){
        $input['join'] = array('nominations_projects','nominations_projects.project_id = projects.id');
        $input['limit'] = array(8,0);
        $input['order'] = array('created','DESC');
        $favourite = $this->Project->joinroom($input);
        if($favourite){
            $this->response($favourite, 200); 
        }else{
            $data = array([
                'status' => ' Get failed!'
            ]);
            $this->response($data, 400); 
        }
    }
    public function expired_get(){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:m:s');

        $input['where'] = array('collection_end_date' < $date);
        $test = $this->Project->get_list($input);
        $this->response($test, 200); 
    }

    public function project_goal_get(){
        $a = (int) '12312312312';
        $input['where'] = array('collected_amount' >= $a);
        $test = $this->Project->get_list($input);
        var_dump($test);
        exit; 
        $this->response($test, 200); 
    }



}
