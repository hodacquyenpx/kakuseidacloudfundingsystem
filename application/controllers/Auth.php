<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Auth extends BD_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('M_main');
        $this->load->library(array('ion_auth', 'form_validation'));
    }
    public function Login_post()
    {
        $kunci = $this->config->item('thekey');
        $invalidLogin = ['status' => 'Invalid Login'];
        if ($this->ion_auth->login($this->post('email'), $this->post('password')))
        {
            $group = $this->ion_auth->get_users_groups()->row();
            $user = $this->ion_auth->user()->row();
            $token['id'] = $user->id;  //From here$g
            $token['role'] = $group->id;  //From here
            $token['username'] = $this->post('email');
            $date = new DateTime();
            $token['iat'] = $date->getTimestamp();
            $token['exp'] = $date->getTimestamp() + 60*60*5; //To here is to generate token
            $output['token'] = JWT::encode($token,$kunci ); //This is the output token
            $output['exp'] = $date->getTimestamp() + 60*60*5;
            $output['_base'] = $group->name;
            $this->response($output, REST_Controller::HTTP_OK); //This is the respon if success
        }
        else
        {
            $this->response($invalidLogin, REST_Controller::HTTP_BAD_REQUEST); //This is the respon if failed
        }
    }


    // Function khong can login 
        // Confirm Email

    // Tạo User
    public function Create_user_post()
    {
        $tables = $this->config->item('tables', 'ion_auth');
        $this->form_validation->set_rules(
            'email', $this->lang->line('create_user_validation_email_label'), 
            'trim|required|valid_email|is_unique[' . $tables['users'] . '.email]');
        $this->form_validation->set_data($this->post());
        if ($this->form_validation->run() === TRUE)
        {
            $email = strtolower($this->post('email'));
            $identity = $this->post('first_name'). $this->post('last_name');
            $password = $this->post('password');

            $additional_data = array(
            'first_name' => $this->post('first_name'),
            'last_name' => $this->post('last_name'),
            'company' => $this->post('company'),
            'phone' => $this->post('phone'),
            );
            //register with data rêcvive
            $new_user =  $this->ion_auth->register($identity, $password, $email, $additional_data);
            //if success
            if($new_user){
                //use json_encode show data.
                $this->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = "hodacquyenpx@gmail.com";
                $config['smtp_pass'] = "quyen123";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";

                $this->email->initialize($config);

                $this->email->from('hodacquyenpx@gmail.com', 'Blabla');
                // $list = array('hodacquyenpx@gmail.com');
                $this->email->to($email);
                $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                $this->email->subject('This is an email test');
                $this->email->message('http://118.27.14.50:83/Auth/Confirm_Email/'.$email);
                if($this->email->send()){
                    $user = array([
                        'status' => 'Create User Success and Send Mail Success !'
                    ]);
                    $this->response($user, REST_Controller::HTTP_OK);
                }else{
                    $invalid = ['status' => 'Create user failed'];
                    $this->response($invalid,  REST_Controller::HTTP_BAD_REQUEST);//401
                }
            }else{
                $invalid = ['status' => 'Create user failed'];
                $this->response($invalid,  REST_Controller::HTTP_BAD_REQUEST);//401
            }
        }else{
            $invalid = ['status' => 'Email is exist'];
            $this->response($invalid,  REST_Controller::HTTP_BAD_REQUEST);//401
        }
    }
    //Confirm Email
    public function Confirm_Email_get($email)
    {
        $user = $this->M_main->getdatavsemail($email)->row();
        $data = array(
            'active' => 1,
        );
        if($user->active == 1){
            $data = array(
                "status" => "This account has been active"
            );
            $this->response($data, REST_Controller::HTTP_NOT_FOUND);
        }else{
            $update = $this->M_main->update($user->id,$data);
            if($update){
                $data = array(
                    "status" => "Active account Success"
                );
                $this->response($data, REST_Controller::HTTP_OK);
            }
        }
   
    }
    public function Forgot_pass_post()
    {   
        $email = $this->post('email');
        $code = $this->ion_auth->forgotten_password($email);
        if($code){
            // config email
            $this->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "hodacquyenpx@gmail.com";
            $config['smtp_pass'] = "quyen123";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";
            
            $this->email->initialize($config);
            
            $this->email->from('aaaaa@gmail.com', 'Blabla');
            $this->email->to($email);
            $this->email->reply_to('hodacquyenpx@gmail.com', 'Explendid Videos');
            $this->email->subject('This is an email test');
            $this->email->message('Please Click Link Below '.'<br>'.'To reset your password to: resetpassword'.'<br>'.'http://118.27.14.50:83/Auth/reset_pass/'.$code['forgotten_password_code']);
            if($this->email->send()){   
                $data = array('status: '.'Send Mail Thành Công');
                $this->response($data, 200);
            }else{
                $data = array('status: '.'Send Mail Thất Bại');
                $this->response($data, REST_Controller::HTTP_BAD_REQUEST);
            }
   
        }else{
            $this->set_response("This User not exits", REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    public function Reset_pass_post($code){
        $user = $this->ion_auth->forgotten_password_check($code);
		if ($user)
		{
            $new = $this->post('new_password');
            $change = $this->ion_auth->reset_password($user->email, $new);

            if ($change)
            {
                $a = $this->ion_auth->forgotten_password_complete($code);
                $data = array([
                    'status:'=> 'change password success!'
                ]);
                $this->response($data, 200);
            }
            else
            {
                $data = array([
                    'status:'=> 'change password failed!'
                ]);
                $this->response($data, REST_Controller::HTTP_BAD_REQUEST);
            }
		}else{
   
            $data = array([
                'status:'=> 'Your Request not found!'
            ]);
            $this->response($data,REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function Reset_pass_get($code){
        $user = $this->ion_auth->forgotten_password_check($code);
		if ($user)
		{
            $new = 'resetpassword';
            $change = $this->ion_auth->reset_password($user->email, $new);

            if ($change)
            {
                $a = $this->ion_auth->forgotten_password_complete($code);
                $data = array([
                    'status:'=> 'change password success!'
                ]);
                $this->response($data, 200);
            }
            else
            {
                $data = array([
                    'status:'=> 'change password failed'
                ]);
                $this->response($data, REST_Controller::HTTP_BAD_REQUEST);
            }
		}else{
            $data = array([
                'status' => 'Your password has been reset'
            ]);
            $this->response($data,REST_Controller::HTTP_NOT_FOUND);
        }
    }
    
}
