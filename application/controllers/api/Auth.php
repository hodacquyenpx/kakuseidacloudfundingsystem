<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Auth extends BD_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('M_main');
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
    }

    public function login_post()
    {
      $config = [
        [
          'field' => 'identity',
          'label' => 'Username',
          'rules' => 'required|min_length[3]',
          'errors' => [
                  'required' => 'We need both username and password',
                  'min_length'=>'Minimum Username length is 3 characters'
          ],
        ],
        [
          'field' => 'password',
          'label' => 'Password',
          'rules' => 'required|min_length[6]',
          'errors' => [
                  'required' => 'You must provide a Password.',
                  'min_length'=>'Minimum Password length is 6 characters',
          ],
        ],
      ];
      $data = $this->post();
      $this->form_validation->set_data($data);
      $this->form_validation->set_rules($config);
            $kunci = $this->config->item('thekey');
                $invalidLogin = ['status' => 'Invalid Login'];
      $remember = (bool)$this->input->post('remember');
      if($this->form_validation->run()==FALSE){
          print_r($this->form_validation->error_array());
      }
      else{
        if ($this->ion_auth->login($this->post('identity'), $this->post('password'), $remember))
        { $user = $this->ion_auth->user()->row();
          $token['id'] = $user->id;  //From here
          $token['username'] = $this->post('identity');
          $date = new DateTime();
          $token['iat'] = $date->getTimestamp();
          $token['exp'] = $date->getTimestamp() + 60*60*5; //To here is to generate token
          $output['token'] = JWT::encode($token,$kunci ); //This is the output token
          $this->set_response($output, REST_Controller::HTTP_OK); //This is the respon if success
          //if the login is successful
          //redirect them back to the home page
          // $this->session->set_flashdata('message', $this->ion_auth->messages());
          // redirect('/', 'refresh');
        }
        else
        {
          // if the login was un-successful
          // redirect them back to the login page
          $this->set_response($invalidLogin, REST_Controller::HTTP_NOT_FOUND); //This is the respon if failed
          // $this->session->set_flashdata('message', $this->ion_auth->errors());
          // redirect('auth/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
        }
      }


    }

    public function lo_post()
    {
      $email = 'ben.edmu1nds@gmail.com';
		$password = '12345678';
		$remember = TRUE; // remember the user
		$var = $this->ion_auth->login($email, $password, $remember);
    var_dump($var);
    exit();

    }
    public function reg_post()
    {
      $username = 'benedmunds';
		$password = '123456781';
		$email = 'ben.ed1munds@gmail.com';
		$additional_data = array(
								'first_name' => 'Ben',
								'last_name' => 'Edmunds',
								);
		$group = array('1'); // Sets user to admin.

		$this->ion_auth->register($username, $password, $email, $additional_data, $group);
    $this->set_response('ok', REST_Controller::HTTP_OK); //This is the respon if success
    }
}
