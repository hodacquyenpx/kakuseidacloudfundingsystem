<?php
require_once APPPATH . '/libraries/REST_Controller.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Example extends REST_Controller {
  function user_get()
     {
         $data = array('returned: '. $this->get('id'));
         $this->response($data);
     }

     function user_post()
     {
         $data = array('returned: '. $this->post('id'));
         $this->response($data);
     }

     function user_put()
     {
         $data = array('returned: '. $this->put('id'));
         $this->response($data);
     }

     function user_delete()
     {
         $data = array('returned: '. $this->delete('id'));
         $this->response($data);
     }
     // AdminController
         // Lấy tất cả các user.
     public function GetAllUser_get()
     {
         $this->load->library('pagination');
         $per_page = (float) $this->get('per_page');
         $user = $this->ion_auth->users()->result();
         $total = count($user);
         // ? $totalPages = ceil($total / $per_page);
 
         $config = array();
         $config['total_rows'] = $total; // tổng tất cả các sản phẩm trong website;
         $config['base_url'] = 'http://118.27.14.50:83/CRUD/GetAllUser?per_page=2'; // link hiển thị dữ liệu
         $config['per_page'] = 1; // số lượng sản phẩm hiện ra;
         $config['uri_segment'] = 4; // hiển thị số trang trên url;
         $config['cur_tag_open'] = '<b>';
         $config['cur_tag_close'] = '</b>';
         $config['next_link'] = 'Next Page';
         $config['prev_link'] = 'Prev Page';
         // khởi tạo  cấu hình phân trang;
 
         $this->pagination->initialize($config);
 
         $segment =$this->uri->segment(4);
         $segment = intval($segment);
 
 
         $input = array();
         $input['limit']  = array($config['per_page'],$segment);
         $list = $this->M_main->get_list($input);
         // $list['bbb'] = $this->pagination->create_links();
         //  if($page <= $totalPages){
         //      var_dump('exit');
         // }
         // if($totalPages >= $page){
         //     var_dump('so trang ban chon la:');
         //     var_dump($page);
         // }else{
         //     var_dump('so trang ban chon khong co!');
         // }
         $user = json_encode($list);
 
         $this->response(json_decode($user), REST_Controller::HTTP_OK);
     }
         // Edit một user với id vs post\
         // Thêm một user mới
    public function create_user_post()
    {
        $email = strtolower($this->post('email'));
        $identity = $this->post('email');
        $password = $this->post('password');

        $additional_data = array(
        'first_name' => $this->post('first_name'),
        'last_name' => $this->post('last_name'),
        'company' => $this->post('company'),
        'phone' => $this->post('phone'),
        );
        //register with data rêcvive
        $new_user =  $this->ion_auth->register($identity, $password, $email, $additional_data);
        //if success
        if($new_user){
            //use json_encode show data.
            $this->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "hodacquyenpx@gmail.com";
            $config['smtp_pass'] = "quyen123";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $this->email->initialize($config);

            $this->email->from('hodacquyenpx@gmail.com', 'Blabla');
            $list = array('hodacquyenpx@gmail.com');
            $this->email->to($list);
            $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
            $this->email->subject('This is an email test');
            $this->email->message('http://118.27.14.50:83/Not_Login/Confirm_Email?email='.$email);
            if($this->email->send()){
                $user = json_encode($this->ion_auth->user($id)->row());
                $this->response(json_decode($user), REST_Controller::HTTP_OK);
            }else{
                $invalid = ['status' => 'Create user failed'];
                $this->response($invalid,  REST_Controller::HTTP_BAD_REQUEST);//401
            }
        }else{
            $invalid = ['status' => 'Create user failed'];
            $this->response($invalid,  REST_Controller::HTTP_BAD_REQUEST);//401
        }
    }

    public function edit_user_post()
	{
        $id = $this->post('id');
		$this->data['title'] = $this->lang->line('edit_user_heading');

		if (!($this->ion_auth->user($id)->row()))
		{
            $data = array('status: '=>'This Id not Exist');
            $this->response($data, REST_Controller::HTTP_BAD_REQUEST);
        }

		$user = $this->ion_auth->user($id)->row();
		$groups = $this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'trim|required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'trim|required');
		$this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'trim|required');
		$this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'trim|required');
        $_POST = $this->post();
		if (isset($_POST) && !empty($_POST))
		{
			// update the password if it was posted
			if ($this->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}
            $this->form_validation->set_data($this->post());
			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
					'first_name' => $this->post('first_name'),
					'last_name' => $this->post('last_name'),
					'company' => $this->post('company'),
					'phone' => $this->post('phone'),
				);

				// update the password if it was posted
				if ($this->post('password'))
				{
					$data['password'] = $this->post('password');
				}

				// check to see if we are updating the user
				if ($this->ion_auth->update($user->id, $data))
				{
                    $user = json_encode($this->ion_auth->user($id)->row());
                    $this->response(json_decode($user), REST_Controller::HTTP_OK);
				}
				else
				{
                    $data = array('status' => 'Update Fail!');
                    $this->response($data, REST_Controller::HTTP_UNAUTHORIZED);
				}

			}else{
                $out_put = json_encode($this->form_validation->error_array());
                $this->set_response(json_decode($out_put), REST_Controller::HTTP_BAD_REQUEST);
            }
		}
    }
    // Delete 1 user với post
    public function delete_user_post()
    {
        $id = (int) $this->post('id');

		if (!($this->ion_auth->user($id)->row()))
		{
            $data = array('status: '=>'This Id not Exist');
            $this->response($data, REST_Controller::HTTP_BAD_REQUEST);
        }
        $data = $this->ion_auth->user($id)->row();
        if($this->ion_auth->delete_user($id)){
            $user = json_encode($data);
            $this->response(json_decode($user), REST_Controller::HTTP_OK);
        }else{
            $data = array('status' => 'Delete Fail!');
            $this->response($data, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }



    // Edit User với put
	public function edit_user_put()
	{
        $id = $this->put('id');
		$this->data['title'] = $this->lang->line('edit_user_heading');

		if (!($this->ion_auth->user($id)->row()))
		{
            $data = array('status: '=>'This Id not Exist');
            $this->response($data, REST_Controller::HTTP_BAD_REQUEST);
        }

		$user = $this->ion_auth->user($id)->row();
            $data = array(
                'first_name' => $this->put('first_name'),
                'last_name' => $this->put('last_name'),
                'company' => $this->put('company'),
                'phone' => $this->put('phone'),
            );

            // update the password if it was posted
            if ($this->put('password'))
            {
                $data['password'] = $this->put('password');
            }

            // Only allow updating groups if user is admin

            // check to see if we are updating the user
            if ($this->ion_auth->update($user->id, $data))
            {
                $user = json_encode($this->ion_auth->user($id)->row());
                $this->response(json_decode($user), REST_Controller::HTTP_OK);
            }
            else
            {
                $data = array('status' => 'Update Fail!');
                $this->response($data, REST_Controller::HTTP_UNAUTHORIZED);
            }
    }

    // Delete user với delete
    public function delete_user_delete($id)
    {
        if (!($this->ion_auth->user($id)->row()))
		{
            $data = array('status: '=>'This Id not Exist');
            $this->response($data, REST_Controller::HTTP_BAD_REQUEST);
        }
        $data = $this->ion_auth->user($id)->row();
        if($this->ion_auth->delete_user($id)){
            $user = json_encode($data);
            $this->response(json_decode($user), REST_Controller::HTTP_OK);
        }else{
            $data = array('status' => 'Delete Fail!');
            $this->response($data, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }


    //Auth Controller
    // Send Mail post
    // public function sendmail_post(){
    //     $this->load->library('email');
    //     $config['protocol'] = "smtp";
    //     $config['smtp_host'] = "ssl://smtp.gmail.com";
    //     $config['smtp_port'] = "465";
    //     $config['smtp_user'] = "hodacquyenpx@gmail.com";
    //     $config['smtp_pass'] = "quyen123";
    //     $config['charset'] = "utf-8";
    //     $config['mailtype'] = "html";
    //     $config['newline'] = "\r\n";

    //     $this->email->initialize($config);

    //     $this->email->from('hodacquyenpx@gmail.com', 'Blabla');
    //     $list = array('hodacquyenpx@gmail.com');
    //     $this->email->to($list);
    //     $this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
    //     $this->email->subject('This is an email test');
    //     $this->email->message('It is working. Great!');
    //     if($this->email->send()){
    //         $this->set_response("Send Mail Thành Công", REST_Controller::HTTP_OK);
    //     }else{
    //         $this->set_response("Send Mail Thất Bại", REST_Controller::HTTP_BAD_REQUEST);
    //     }
    // }

    // Forgot Pass post
    // public function forgot_pass_post()
    // {
    //     $email = $this->post('email');
    //     $code = $this->ion_auth->forgotten_password($email);
    //     if($code){
    //         // config email
    //         $this->load->library('email');
    //         $config['protocol'] = "smtp";
    //         $config['smtp_host'] = "ssl://smtp.gmail.com";
    //         $config['smtp_port'] = "465";
    //         $config['smtp_user'] = "hodacquyenpx@gmail.com";
    //         $config['smtp_pass'] = "quyen123";
    //         $config['charset'] = "utf-8";
    //         $config['mailtype'] = "html";
    //         $config['newline'] = "\r\n";

    //         $this->email->initialize($config);

    //         $this->email->from('aaaaa@gmail.com', 'Blabla');
    //         $this->email->to($email);
    //         $this->email->reply_to('hodacquyenpx@gmail.com', 'Explendid Videos');
    //         $this->email->subject('This is an email test');
    //         $this->email->message('http://118.27.14.50:83/CRUD_Admin/reset_pass/'.$code['forgotten_password_code']);
    //         if($this->email->send()){
    //             $data = array('status: '.'Send Mail Thành Công');
    //             $this->response($data, 200);
    //         }else{
    //             $data = array('status: '.'Send Mail Thất Bại');
    //             $this->response($data, REST_Controller::HTTP_BAD_REQUEST);
    //         }

    //     }else{
    //         $this->set_response("This User not exits", REST_Controller::HTTP_BAD_REQUEST);
    //     }
    // }
    // public function reset_pass_post($code){
    //     $new = $this->post('new');
    //     $user = $this->ion_auth->forgotten_password_check($code);
	// 	if ($user)
	// 	{
    //         $change = $this->ion_auth->reset_password($user->email, $new);

    //         if ($change)
    //         {
    //             $a = $this->ion_auth->forgotten_password_complete($code);
    //             $data = array([
    //                 'email:'=> $user->email,
    //                 'data'=>$a
    //             ]);
    //             $this->response($data, 200);
    //         }
    //         else
    //         {
    //             $data = array([
    //                 'status:'=> 'change password failed'
    //             ]);
    //             $this->response($data, REST_Controller::HTTP_BAD_REQUEST);
    //         }
	// 	}else{

    //         $this->response( REST_Controller::HTTP_NOT_FOUND);
    //     }
    // }

}
?>
