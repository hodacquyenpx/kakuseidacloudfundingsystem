-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 19, 2018 at 08:40 AM
-- Server version: 5.5.60-MariaDB
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kakuseida-test`
--

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE IF NOT EXISTS `areas` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'エリア（カテゴリ２）名',
  `order` int(2) NOT NULL DEFAULT '1' COMMENT '表示順'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='エリア（カテゴリ２）';

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `name`, `order`) VALUES
(1, '関東', 1),
(2, '北海道', 1),
(3, '東北', 1),
(4, '中部', 1),
(5, '近畿', 1),
(6, '中国', 1),
(7, '四国', 1),
(8, '九州・沖縄', 1),
(9, '日本全国', 1),
(10, '全世界', 1);

-- --------------------------------------------------------

--
-- Table structure for table `backed_projects`
--

CREATE TABLE IF NOT EXISTS `backed_projects` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL COMMENT 'プロジェクトID',
  `user_id` int(11) DEFAULT NULL COMMENT 'ユーザID',
  `backing_level_id` int(11) DEFAULT NULL COMMENT '支援パターンID',
  `invest_amount` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '支援金額',
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '支援コメント',
  `stripe_charge_id` varchar(255) DEFAULT NULL COMMENT 'StripeのChargeのID',
  `status` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '作成完了' COMMENT '決済ステータス',
  `created` datetime DEFAULT NULL COMMENT 'データ作成日時',
  `modified` datetime DEFAULT NULL COMMENT 'データ更新日時',
  `manual_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '手動登録フラグ'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='決済データ';

--
-- Dumping data for table `backed_projects`
--

INSERT INTO `backed_projects` (`id`, `project_id`, `user_id`, `backing_level_id`, `invest_amount`, `comment`, `stripe_charge_id`, `status`, `created`, `modified`, `manual_flag`) VALUES
(6, 4, 2, 5, '5000', '一緒に覚醒したいっす！', 'ch_1CwNizJXon7TV5C6uc5i96W9', '売上確定', '2018-08-07 14:27:19', '2018-08-07 14:27:19', 0),
(7, 4, 21, 5, '200000', '今度クラウドファンディングを検討しています。\r\n是非相談に乗ってください。', 'ch_1CwOHVJXon7TV5C6dlJ8cUNG', '売上確定', '2018-08-07 15:02:58', '2018-08-07 15:02:58', 0),
(8, 5, 23, 7, '2000', '大変興味があります。頑張ってください。', 'ch_1Cx4caJXon7TV5C6ijnq4BXi', '売上確定', '2018-08-09 12:15:34', '2018-08-09 12:15:34', 0);

-- --------------------------------------------------------

--
-- Table structure for table `backing_levels`
--

CREATE TABLE IF NOT EXISTS `backing_levels` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL COMMENT 'プロジェクトID',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '支援パターン名',
  `invest_amount` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT '最低支援額',
  `return_amount` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'リターン内容',
  `max_count` int(5) DEFAULT NULL COMMENT '最大支援数',
  `now_count` int(5) NOT NULL DEFAULT '0' COMMENT '現在の支援数',
  `delivery` int(1) DEFAULT '1' COMMENT 'リターン方法',
  `created` datetime DEFAULT NULL COMMENT 'データ作成日時',
  `modified` datetime DEFAULT NULL COMMENT 'データ更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='支援パターン（リターン内容）';

--
-- Dumping data for table `backing_levels`
--

INSERT INTO `backing_levels` (`id`, `project_id`, `name`, `invest_amount`, `return_amount`, `max_count`, `now_count`, `delivery`, `created`, `modified`) VALUES
(5, 4, 'level 1', '5000', 'クラウドファンディングプロジェクトの企画内容、写真、動画等送っていただけたらアップロード手続きの代行をいたします。また、クラウドファンディングに関するアドバイス等も致します。', NULL, 2, 1, '2018-08-07 11:21:23', '2018-08-09 12:10:32'),
(7, 5, 'level 1', '2000', 'ご支援いただけた方には、大阪の寺院、浄信寺で行っている研究講義の内容を毎月メールで配信させていただきます。', NULL, 1, 1, '2018-08-09 12:00:59', '2018-08-09 12:15:32'),
(8, 8, 'level 1', '123123', 'sđasad', 2147483647, 0, 2, '2018-08-15 18:03:53', '2018-08-15 18:03:53'),
(9, 8, 'level 2', '123123', '123123sdsad', 123123, 0, 2, '2018-08-15 18:03:53', '2018-08-15 18:03:53');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `order` int(2) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `order`, `slug`, `created_at`, `updated_at`) VALUES
(8, 'name', 1, '1', '2018-09-19 10:09:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `favourite_projects`
--

CREATE TABLE IF NOT EXISTS `favourite_projects` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT 'ユーザID',
  `project_id` int(11) DEFAULT NULL COMMENT 'プロジェクトID',
  `backed` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL COMMENT 'データ生成日時'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='お気に入りプロジェクト';

--
-- Dumping data for table `favourite_projects`
--

INSERT INTO `favourite_projects` (`id`, `user_id`, `project_id`, `backed`, `created`) VALUES
(1, 1, 4, '', '2018-08-15 01:46:56'),
(2, 1, 14, '', '2018-09-19 08:00:00'),
(3, 1, 15, '', '2018-09-19 08:00:00'),
(4, 1, 5, '', '2018-09-19 08:00:00'),
(5, 1, 8, '', '2018-09-19 08:00:00'),
(6, 1, 9, '', '2018-09-19 08:00:00'),
(7, 1, 10, '', '2018-09-19 08:00:00'),
(8, 1, 11, '', '2018-09-19 08:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `News`
--

CREATE TABLE IF NOT EXISTS `News` (
  `id` int(30) NOT NULL,
  `title` varchar(255) NOT NULL,
  `des` varchar(255) NOT NULL,
  `thumnail` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `News`
--

INSERT INTO `News` (`id`, `title`, `des`, `thumnail`, `content`, `created`, `updated`) VALUES
(1, 'Test title1', 'decription of test title1', 'https://via.placeholder.com/350x150', 'content of test title1', '2018-09-19 10:22:00', '0000-00-00 00:00:00'),
(2, 'Test title4', 'decription of test title4', 'https://via.placeholder.com/350x150', 'content of test title4', '2018-09-19 13:22:00', '0000-00-00 00:00:00'),
(3, 'Test title2', 'decription of test title2', 'https://via.placeholder.com/350x150', 'content of test title2', '2018-09-19 10:22:00', '0000-00-00 00:00:00'),
(4, 'Test title3', 'decription of test title3', 'https://via.placeholder.com/350x150', 'content of test title3', '2018-09-19 12:22:00', '0000-00-00 00:00:00'),
(5, 'Test title5', 'decription of test title5', 'https://via.placeholder.com/350x150', 'content of test title5', '2018-09-19 10:22:00', '0000-00-00 00:00:00'),
(6, 'Test title6', 'decription of test title6', 'https://via.placeholder.com/350x150', 'content of test title6', '2018-09-19 11:22:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `nominations_projects`
--

CREATE TABLE IF NOT EXISTS `nominations_projects` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `backed` varchar(20) NOT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nominations_projects`
--

INSERT INTO `nominations_projects` (`id`, `user_id`, `project_id`, `backed`, `created`) VALUES
(1, 1, 4, '', '2018-09-19 10:00:00'),
(2, 1, 5, '', '2018-09-19 12:00:00'),
(3, 1, 8, '', '2018-09-19 09:29:00'),
(4, 1, 9, '', '2018-09-19 14:22:00'),
(5, 1, 10, '', '2018-09-19 10:43:00'),
(6, 1, 11, '', '2018-09-19 15:00:00'),
(7, 1, 12, '', '2018-09-19 08:00:00'),
(8, 1, 15, '', '2018-09-19 14:30:00'),
(9, 1, 14, '', '2018-09-19 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pickup_projects`
--

CREATE TABLE IF NOT EXISTS `pickup_projects` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `backed` varchar(20) NOT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pickup_projects`
--

INSERT INTO `pickup_projects` (`id`, `user_id`, `project_id`, `backed`, `created`) VALUES
(1, 1, 4, '', '2018-09-19 09:00:00'),
(2, 1, 5, '', '2018-09-19 09:13:00'),
(3, 1, 9, '', '2018-09-19 09:13:00'),
(4, 1, 10, '', '2018-09-19 09:13:00'),
(5, 1, 11, '', '2018-09-19 09:13:00'),
(6, 1, 12, '', '2018-09-19 09:13:00'),
(7, 1, 13, '', '2018-09-19 09:13:00'),
(9, 1, 14, '', '2018-09-19 09:27:00');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL,
  `setting_id` int(10) NOT NULL,
  `project_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'プロジェクト名',
  `category_id` int(11) DEFAULT NULL COMMENT 'カテゴリ１ID',
  `area_id` int(10) DEFAULT NULL COMMENT 'エリアID',
  `description` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'プロジェクト概要',
  `goal_amount` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '目標金額',
  `collection_term` int(2) NOT NULL DEFAULT '60' COMMENT '募集期間（日）',
  `collection_start_date` datetime DEFAULT NULL COMMENT '募集開始日時',
  `collection_end_date` datetime DEFAULT NULL COMMENT '募集終了日時',
  `thumbnail_type` int(1) NOT NULL DEFAULT '1' COMMENT 'サムネイル種別',
  `thumbnail_movie_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT 'youtube' COMMENT 'サムネイルの動画種別',
  `thumbnail_movie_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'サムネイルの動画コード',
  `user_id` int(11) DEFAULT NULL COMMENT 'ユーザID',
  `opened` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no' COMMENT '公開ステータス',
  `backers` int(11) DEFAULT '0' COMMENT '支援者数',
  `comment_cnt` int(4) NOT NULL DEFAULT '0' COMMENT 'コメント数',
  `report_cnt` int(3) NOT NULL DEFAULT '0' COMMENT '活動報告数',
  `collected_amount` int(10) DEFAULT '0' COMMENT '現在の支援総額',
  `max_back_level` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '支援パターン数',
  `created` datetime DEFAULT NULL COMMENT 'データ作成日時',
  `modified` datetime DEFAULT NULL COMMENT 'データ更新日時',
  `active` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes' COMMENT '募集中',
  `stop` tinyint(1) NOT NULL DEFAULT '0' COMMENT '公開停止ステータス',
  `return` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'リターン概要',
  `contact` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'プロジェクト起案者の連絡先',
  `rule` tinyint(1) NOT NULL DEFAULT '0' COMMENT '利用規約同意有無',
  `site_fee` int(2) DEFAULT NULL COMMENT 'サイト手数料率',
  `site_price` int(10) NOT NULL DEFAULT '0' COMMENT 'サイト手数料',
  `project_owner_price` int(10) NOT NULL DEFAULT '0' COMMENT 'プロジェクト起案者への支払額'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='プロジェクト';

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `setting_id`, `project_name`, `category_id`, `area_id`, `description`, `goal_amount`, `collection_term`, `collection_start_date`, `collection_end_date`, `thumbnail_type`, `thumbnail_movie_type`, `thumbnail_movie_code`, `user_id`, `opened`, `backers`, `comment_cnt`, `report_cnt`, `collected_amount`, `max_back_level`, `created`, `modified`, `active`, `stop`, `return`, `contact`, `rule`, `site_fee`, `site_price`, `project_owner_price`) VALUES
(4, 0, 'カクセイだ！スタートアッププロジェクト', 13, 9, '手数料無料のクラウドファンディング、カクセイだ！のフラッグシッププロジェクトです。\r\n全ての人にクラウドファンディングを！全ての人がやりたいことをやれる世界、それぞれの人がやりたいことをみんなに応援してもらえる環境作りを目指しています。\r\nクラウドファンディングに挑戦してみたいけど、やり方がわからない。企画をアップロードする方法がわからない。どうやって宣伝したらいいのかわからない。そんな人の代わりにカクセイだ！へのアップロード代行を致します。\r\nやりたい気持ちを表現すれば、誰もがチャンスをもらえる時代を我々は共に歩みます。', '500000', 60, '2018-08-07 11:23:20', '2018-10-06 11:23:20', 1, 'youtube', NULL, 1, 'yes', 2, 0, 0, 205000, '1', '2018-08-07 11:18:42', '2018-08-09 12:10:32', 'yes', 0, 'クラウドファンディング企画のアップロード代行\r\nアドバイスなど', 'kakuseida@kakuseida.com', 1, 0, 0, 0),
(5, 0, '研究講義＠浄信寺　「歎異抄に問う、宗教は死んだか」', 11, 5, '宗教は厄介物か、宗教をめぐる世界中の度重なる紛争、本邦では記憶に新しいオウム真理教をめぐる事件の数々などがネガティブな事象が容易に思い浮かびますが、宗教に助けられたという話は全く聞きません。とくに伝統宗教においてはです。\r\nそんな時代に私、浄土真宗大谷派寺院の住職が積年考え抜いた宗教の必要性を浄土真宗のきっての聖典「歎異抄」を読み進めながら探求していこうという研究講義開催の試みです。\r\n「歎異抄」というのは浄土真宗の宗祖親鸞聖人の言葉をつづった書物であり、あの有名な「善人さえ助かるのだから悪人はなおさらだ」という意味の悪人正機説を含んだ書物であります。賛否両論あり、その解説書を書くような浄土真宗の学者でさえも、間違った解釈をしているなどと批評されることもよくあります。が、そもそも解釈なんぞ個人個人違ってしかるべき、解釈のコンセンサスが必要なものでもあるまいというのが私の見解であり、その信念をもって歎異抄を読み進めていきながら、今の時代での宗教の在り方を掘り下げる研究講義を行うものであります。', '200000', 60, '2018-08-09 12:01:30', '2018-10-08 12:01:30', 1, 'youtube', '', 21, 'yes', 1, 0, 0, 2000, '1', '2018-08-09 11:44:23', '2018-08-15 17:59:54', 'yes', 0, 'ご支援いただけた方には、大阪の寺院、浄信寺で行っている研究講義の内容を毎月メールで配信させていただきます。', 'joshinji@shakutaikan.com', 1, 0, 0, 0),
(8, 0, 'test', 9, 4, 'áđasad', '123123', 60, '2018-08-15 18:04:08', '2018-10-14 18:04:08', 1, 'youtube', '', 24, 'yes', 0, 1, 0, 0, '2', '2018-08-15 18:02:06', '2018-08-15 18:13:50', 'yes', 0, 'áđá', 'áđâsđâsd', 1, 0, 0, 0),
(9, 1, 'test2', 8, 5, 'test description', '20000', 60, '2018-09-19 09:00:00', '2018-11-18 09:00:00', 1, 'youtube', NULL, 1, 'yes', 0, 0, 0, 1111, '1', '2018-09-19 09:00:00', NULL, 'yes', 0, 'test return', 'hodacquyenpx@gmail.com', 1, 0, 0, 0),
(10, 1, 'test333', 8, 5, 'test description', '20000', 60, '2018-09-19 09:00:00', '2018-09-18 10:00:00', 1, 'youtube', NULL, 1, 'yes', 0, 0, 0, 1111, '1', '2018-09-19 17:00:00', NULL, 'yes', 0, 'test return', 'hodacquyenpx@gmail.com', 1, 0, 0, 0),
(11, 1, 'test5', 8, 5, 'test description', '20000', 60, '2018-09-19 10:00:00', '2018-11-18 09:00:00', 1, 'youtube', NULL, 1, 'yes', 0, 0, 0, 1111, '1', '2018-09-19 16:00:00', NULL, 'yes', 0, 'test return', 'hodacquyenpx@gmail.com', 1, 0, 0, 0),
(12, 1, 'test6', 8, 5, 'test description', '20000', 60, '2018-09-19 09:00:00', '2018-11-18 09:00:00', 1, 'youtube', NULL, 1, 'yes', 0, 0, 0, 1111, '1', '2018-09-19 10:00:00', NULL, 'yes', 0, 'test return', 'hodacquyenpx@gmail.com', 1, 0, 0, 0),
(13, 1, 'test7', 8, 5, 'test description', '20000', 60, '2018-09-19 13:00:00', '2018-11-18 09:00:00', 1, 'youtube', NULL, 1, 'yes', 0, 0, 0, 1111, '1', '2018-09-19 11:00:00', NULL, 'yes', 0, 'test return', 'hodacquyenpx@gmail.com', 1, 0, 0, 0),
(14, 1, 'test8', 8, 5, 'test description', '20000', 60, '2018-09-19 14:00:00', '2018-11-18 09:00:00', 1, 'youtube', NULL, 1, 'yes', 0, 0, 0, 1111, '1', '2018-09-19 13:00:00', NULL, 'yes', 0, 'test return', 'hodacquyenpx@gmail.com', 1, 0, 0, 0),
(15, 1, 'testingt2', 8, 5, 'test description', '20000', 60, '2018-09-19 17:00:00', '2018-11-18 09:00:00', 1, 'youtube', NULL, 1, 'yes', 0, 0, 0, 1111, '1', '2018-09-19 13:00:00', NULL, 'yes', 0, 'test return', 'hodacquyenpx@gmail.com', 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `project_contents`
--

CREATE TABLE IF NOT EXISTS `project_contents` (
  `id` int(10) NOT NULL,
  `project_id` int(10) NOT NULL COMMENT 'プロジェクトID',
  `open` int(4) NOT NULL DEFAULT '0' COMMENT '公開ステータス',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text' COMMENT 'コンテンツ種別',
  `txt_content` text COLLATE utf8_unicode_ci COMMENT 'テキストコンテンツ内容',
  `movie_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'youtube' COMMENT '動画種別',
  `movie_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '動画コード',
  `img_caption` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '画像キャプション'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='プロジェクト詳細のコンテンツ';

--
-- Dumping data for table `project_contents`
--

INSERT INTO `project_contents` (`id`, `project_id`, `open`, `type`, `txt_content`, `movie_type`, `movie_code`, `img_caption`) VALUES
(4, 4, 1, 'text', '<span style="font-size:20px;">手数料無料のクラウドファンディング、カクセイだ！のフラッグシッププロジェクトです。</span>\r\n全ての人にクラウドファンディングを！全ての人がやりたいことをやれる世界、それぞれの人がやりたいことをみんなに応援してもらえる環境作りを目指しています。\r\nクラウドファンディングに挑戦してみたいけど、やり方がわからない。企画をアップロードする方法がわからない。どうやって宣伝したらいいのかわからない。そんな人の代わりにカクセイだ！へのアップロード代行を致します。\r\nやりたい気持ちを表現すれば、誰もがチャンスをもらえる時代を我々は共に歩みます。', 'youtube', NULL, NULL),
(5, 4, 1, 'img', NULL, 'youtube', NULL, ''),
(6, 5, 1, 'text', '宗教は厄介物か？\r\n宗教をめぐる世界中の度重なる紛争、本邦では記憶に新しいオウム真理教をめぐる事件の数々などがネガティブな事象が容易に思い浮かびますが、宗教に助けられたという話は全く聞きません。とくに伝統宗教においてはです。\r\nそんな時代に私、浄土真宗大谷派寺院の住職が積年考え抜いた宗教の必要性を浄土真宗のきっての聖典「歎異抄」を読み進めながら探求していこうという研究講義開催の試みです。\r\n「歎異抄」というのは浄土真宗の宗祖親鸞聖人の言葉をつづった書物であり、あの有名な「善人さえ助かるのだから悪人はなおさらだ」という意味の悪人正機説を含んだ書物であります。賛否両論あり、その解説書を書くような浄土真宗の学者でさえも、間違った解釈をしているなどと批評されることもよくあります。が、そもそも解釈なんぞ個人個人違ってしかるべき、解釈のコンセンサスが必要なものでもあるまいというのが私の見解であり、その信念をもって歎異抄を読み進めていきながら、今の時代での宗教の在り方を掘り下げる研究講義を行うものであります。\r\n\r\n<span style="font-weight:bold;">支援金の使い道：</span>\r\n研究・講義に必要な取材費、書物費、交通費', 'youtube', NULL, NULL),
(8, 5, 1, 'img', NULL, 'youtube', NULL, ''),
(9, 5, 1, 'img', NULL, 'youtube', NULL, '宗祖親鸞聖人'),
(10, 5, 1, 'img', NULL, 'youtube', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `project_content_orders`
--

CREATE TABLE IF NOT EXISTS `project_content_orders` (
  `project_id` int(10) NOT NULL COMMENT 'プロジェクトID',
  `order` text COLLATE utf8_unicode_ci NOT NULL COMMENT '表示順'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='プロジェクト詳細のコンテンツの表示順';

--
-- Dumping data for table `project_content_orders`
--

INSERT INTO `project_content_orders` (`project_id`, `order`) VALUES
(4, '["4","5"]'),
(5, '["6","9","8","10"]');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `nick_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'ニックネーム',
  `sex` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '性別',
  `address` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'お住まい',
  `birthday` date DEFAULT NULL COMMENT '生年月日',
  `birth_area` varchar(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `school` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'twitter ID',
  `facebook_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Facebook ID',
  `self_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '自己紹介',
  `url1` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'URL1',
  `url2` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'URL2',
  `url3` varchar(200) CHARACTER SET latin1 DEFAULT NULL COMMENT 'URL3',
  `receive_address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT 'リターン受け取り先住所',
  `token` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'トークン',
  `modified` datetime DEFAULT NULL COMMENT 'データ更新日時'
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `nick_name`, `sex`, `address`, `birthday`, `birth_area`, `school`, `twitter_id`, `facebook_id`, `self_description`, `url1`, `url2`, `url3`, `receive_address`, `token`, `modified`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1537320970, 1, 'Admin', 'istrator', 'ADMIN', '0', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '', 'YASU', '56f08497d925709514747e90cca166aa4b848e2b', NULL, 'tips@bbam.jp', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'YASU', NULL, '', NULL, NULL, NULL, '100931071', '2438771586136659', '', '', '', '', '', NULL, '2018-08-10 11:40:42'),
(3, '', '', '95d3bea4e0cb871ce286cb326eb03457f99c0cf8', NULL, 'endo.yuta@logicky.com', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'user10', NULL, '', NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, '2018-07-10 21:47:51'),
(4, '', 'saito', 'b3ff8b0aac19ce108f9054127f3e98f20a5c686d', NULL, 'imlabosns@gmail.com', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'OCDS', NULL, NULL, NULL, NULL, NULL, '973097208739282944', '885927338281760', NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-01 15:48:33'),
(5, '', 'test taro', '95d3bea4e0cb871ce286cb326eb03457f99c0cf8', NULL, 'info@logicky.com', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'fb', NULL, '', NULL, NULL, NULL, NULL, '', '\r\n', '', '', '', '', NULL, '2018-07-17 22:26:21'),
(6, '', 'kakusei従業員', 'fff99191065b6941963d42b8c33f7db5444721b9', NULL, 'yamashita@kakusei.com', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, '321782451694023', NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-20 14:06:06'),
(12, '', 'Iku Kuru', 'fa8a9d7c43ce27e3c35edbca5318f04431ff716a', NULL, 'ichtanzemitdir@yahoo.co.jp', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'Ikue', NULL, NULL, NULL, NULL, NULL, NULL, '2229424133753164', NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-14 17:14:48'),
(13, '', NULL, '95d3bea4e0cb871ce286cb326eb03457f99c0cf8', NULL, 'edo96815@gmail.com', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'edo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-16 21:42:59'),
(14, '', NULL, '5e38aea99c00ab17fd3cb8a039f4d899a3ad6a8b', NULL, 'trrer2@yahoo.co.jp', NULL, NULL, NULL, NULL, 2018, NULL, 0, NULL, NULL, NULL, NULL, 'trrerY', NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-07 15:13:53'),
(15, '', '不動産のカクセイ', 'a67eb91ab1d3960269b81b4e77059308c316482f', NULL, 'trrer2@gmail.com', NULL, NULL, NULL, NULL, 2018, NULL, 0, NULL, NULL, NULL, NULL, 'kakuseitokyo', NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-07 15:13:37'),
(16, '', 'test jiro', '95d3bea4e0cb871ce286cb326eb03457f99c0cf8', NULL, 'hrkhrkhrk50@gmail.com', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'test jiro', NULL, '', NULL, NULL, NULL, NULL, '138543003702376', '', '', '', '', '', NULL, '2018-07-17 22:36:42'),
(17, '', 'Taro Yamada', 'a67eb91ab1d3960269b81b4e77059308c316482f', NULL, 'uripika@uripika.net', NULL, NULL, NULL, NULL, 2018, NULL, 0, NULL, NULL, NULL, NULL, 'taroyamada', NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-07 15:14:03'),
(18, '', 'KAGEMASA MOHRI', 'a67eb91ab1d3960269b81b4e77059308c316482f', NULL, 'fumikiriboy@outlook.com', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'fumikiri', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-09 12:10:10'),
(19, '', 'Cai Xin', '82358ec34c0db54e3a71fa857d8cc565086fd0c9', NULL, 'caixin007@hotmail.com', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'Cai', NULL, NULL, NULL, NULL, NULL, NULL, '10213188793257203', NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-20 11:50:11'),
(20, '', NULL, 'f330d64f26e69ce16561b2fc0c580407ac645c55', NULL, 'oda.mikio@gmail.com', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, '7thCode.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-07 12:47:07'),
(21, '', 'Shaku Taikan', '71b958c20d278ad741015868884cf4edef651fe2', NULL, 'joshinji@shakutaikan.com', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'T Shaku', NULL, '大阪府堺市浜寺元町3－330', NULL, NULL, NULL, NULL, NULL, '浄土真宗大谷派　寺院\r\n安楽山　浄信寺\r\n住職　釈泰寛', '', '', '', '', NULL, '2018-08-10 11:36:46'),
(22, '', NULL, '90b9b500dadee35352b8522ddd85f26e1b3b8838', NULL, 'torikai@kakusei.com', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'yuppi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-07 17:24:37'),
(23, '', 'KAGEMASA MOHRI', 'a67eb91ab1d3960269b81b4e77059308c316482f', NULL, 'trrer2@gmail.com', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'TRR', NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, '2018-08-09 12:23:38'),
(24, '', '', '5d11ca5c23e56705bd5f49073128e15e0253aace', NULL, 'tmntester001@gmail.com', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'tmntester', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-15 16:20:30'),
(25, '', 'admin', '9ce1d5eb4ebf6ef6343fa0652e0aa8e6013f3972', NULL, 'kakuseida@kakuseida.com', NULL, NULL, NULL, NULL, 2018, NULL, 1, NULL, NULL, NULL, NULL, 'カクセイだ！企画', NULL, '東京都渋谷区', NULL, NULL, NULL, NULL, NULL, '', 'http://www.kakusei.com/', '', '', '', NULL, '2018-08-15 18:37:23');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backed_projects`
--
ALTER TABLE `backed_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backing_levels`
--
ALTER TABLE `backing_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favourite_projects`
--
ALTER TABLE `favourite_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `News`
--
ALTER TABLE `News`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nominations_projects`
--
ALTER TABLE `nominations_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pickup_projects`
--
ALTER TABLE `pickup_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_contents`
--
ALTER TABLE `project_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_content_orders`
--
ALTER TABLE `project_content_orders`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `backed_projects`
--
ALTER TABLE `backed_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `backing_levels`
--
ALTER TABLE `backing_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `favourite_projects`
--
ALTER TABLE `favourite_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `News`
--
ALTER TABLE `News`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `nominations_projects`
--
ALTER TABLE `nominations_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `pickup_projects`
--
ALTER TABLE `pickup_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `project_contents`
--
ALTER TABLE `project_contents`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `project_content_orders`
--
ALTER TABLE `project_content_orders`
  MODIFY `project_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'プロジェクトID',AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
